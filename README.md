## 5. Docker compose
- Склонируйте репозиторий с приложением: [ссылка на GitLab](https://gitlab.com/test9613/5_docker_compose).
- Самостоятельно напишите Dockerfile и docker-compose.yml файлы для сборки и запуска приложения. Можете смело использовать все известные вам способы оптимизации, в том числе конкретно для Python.
- Постарайтесь добиться, чтобы размер вашего контейнера с готовым к работе приложением не превышал **150** мб.
#### Условия задания: 
- Должен использоваться начальный образ c Python версии 3.8
- Само приложение должно запускаться командой `python setup.py && flask run --host=0.0.0.0 --port 8000`. В репозитории есть скрипт docker-entrypoint.sh, можете в качестве стартовой команды запускать его.
- Приложение должно работать от имени пользователя `app`
- Рабочей директорией в контейнере должен быть путь `/app`
- В `Dockerfile` также необходимо передавать дополнительную переменную для работы приложения - `FLASK_APP=src/app.py`
- В `docker-compose.yml` файле должно быть два сервиса - `db (postgres)` и `app`
- Приложение должно собираться автоматически при выполнении команды `docker-compose up`
- Приложение должно запускаться из локальных файлов проекта (то есть нужно монтировать директорию с кодом в рабочую директорию контейнера с приложением)
- Для работы приложения ему нужно передать следующие переменные:
```yaml
DB_NAME: <имя_бд>
DB_USER: <пользователь_бд>
DB_PASSWORD: <пароль_бд>
DB_HOST: db
FLASK_ENV: development
```
Не забудьте согласовать переменные окружения приложения и вашей базы данных.
- Приложение должно уметь сохранять в БД пользователей с `lastName` и `firstName` и выводить данные пользователя по запросу с `id`.
Примеры запросов:
```sh
curl localhost:8000/api/v1/users \
  -X POST -d '{"firstName":"Docker","lastName":"testUser"}' \
  --header "Content-Type: application/json"

curl localhost:8000/api/v1/users/1
```
***
